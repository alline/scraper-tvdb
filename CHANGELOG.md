# [2.0.0](https://gitlab.com/alline/scraper-tvdb/compare/v1.0.2...v2.0.0) (2020-07-09)


### Bug Fixes

* update dependencies ([1c51694](https://gitlab.com/alline/scraper-tvdb/commit/1c516943bf2c0a82ef1c6505548edb704020c69e))


### BREAKING CHANGES

* update @alline/core to v3

## [1.0.2](https://gitlab.com/alline/scraper-tvdb/compare/v1.0.1...v1.0.2) (2020-06-29)


### Bug Fixes

* update dependencies ([1fa360b](https://gitlab.com/alline/scraper-tvdb/commit/1fa360b9b7899d9c9bc061e5cdc80f049eb73bd7))

## [1.0.1](https://gitlab.com/alline/scraper-tvdb/compare/v1.0.0...v1.0.1) (2020-06-29)


### Bug Fixes

* update dependencies ([9c7fe69](https://gitlab.com/alline/scraper-tvdb/commit/9c7fe69b19a281801ec0960eaed7f5fc1543f815))

# 1.0.0 (2020-05-19)


### Features

* initial commit ([81902be](https://gitlab.com/alline/scraper-tvdb/commit/81902be224b4b14155cb4101140cf751c49095ec))
