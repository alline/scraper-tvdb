import _ from "lodash";
import axios, { AxiosInstance } from "axios";
import { Episode } from "@alline/model";
import { EpisodeContext, EpisodeResult, EpisodeScraper } from "@alline/core";
import { AsyncSeriesWaterfallHook } from "tapable";
import moment, { Moment } from "moment";

export interface TvdbEpisodeScraperOption {
  apiKey: string;
  userKey: string;
  userName: string;
  tvdbId: string;
  acceptLanguage: string;
  fields: ("title" | "aired" | "summary" | "thumbnails")[];
}

interface TvdbLoginResponse {
  token: string;
}

interface TvdbQueryResponse {
  links: {
    first: number;
    last: number;
    next?: number;
    prev?: number;
  };
  data: {
    id: number;
    airedSeason: number;
    airedSeasonID: number;
    airedEpisodeNumber: number;
    episodeName: string;
    firstAired: string;
    guestStars: string[];
    directors: string[];
    writers: string[];
    overview: string;
    language: {
      episodeName: string;
      overview: string;
    };
    productionCode: string;
    showUrl: string;
    lastUpdated: number;
    dvdDiscid: string;
    dvdSeason?: number;
    dvdEpisodeNumber?: number;
    dvdChapter?: number;
    absoluteNumber: number;
    filename: string;
    seriesId: number;
    lastUpdatedBy: number;
    airsAfterSeason?: number;
    airsBeforeSeason?: number;
    airsBeforeEpisode?: number;
    imdbId: string;
    contentRating: string;
    thumbAuthor: number;
    thumbAdded: string;
    thumbWidth: string;
    thumbHeight: string;
    siteRating: number;
    siteRatingCount: number;
    isMovie: number;
  };
}

export interface TvdbEpisodeScraperHook {
  transformTitle: AsyncSeriesWaterfallHook<[string[], EpisodeContext]>;
  transformDate: AsyncSeriesWaterfallHook<[Moment, EpisodeContext]>;
  transformSummary: AsyncSeriesWaterfallHook<[string, EpisodeContext]>;
  transformThumbnails: AsyncSeriesWaterfallHook<[string[], EpisodeContext]>;
}

export class TvdbEpisodeScraper extends EpisodeScraper {
  tvdbHooks: TvdbEpisodeScraperHook;
  private axios: AxiosInstance;
  private initToken = false;
  private option: TvdbEpisodeScraperOption;

  constructor(option: TvdbEpisodeScraperOption) {
    super();
    this.option = option;
    this.axios = axios.create({
      baseURL: "https://api.thetvdb.com/",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Accept-Language": this.option.acceptLanguage
      }
    });
    this.tvdbHooks = {
      transformTitle: new AsyncSeriesWaterfallHook(["title", "ctx"]),
      transformDate: new AsyncSeriesWaterfallHook(["aired", "ctx"]),
      transformSummary: new AsyncSeriesWaterfallHook(["summary", "ctx"]),
      transformThumbnails: new AsyncSeriesWaterfallHook(["thumbnails", "ctx"])
    };
  }

  protected async onScrap(
    rlt: EpisodeResult,
    ctx: EpisodeContext
  ): Promise<EpisodeResult> {
    const {
      transformSummary,
      transformTitle,
      transformDate,
      transformThumbnails
    } = this.tvdbHooks;
    const { logger } = ctx;
    await this.initAuthToken(ctx);
    const { data: episodeData } = await this.fetchEpisode(ctx);
    logger.debug("onScrap", { label: "TvdbEpisodeScraper", data: episodeData });
    const { fields } = this.option;
    const data: Partial<Episode> = {};
    let thumbnails: string[] = [];
    if (fields.includes("title") || fields.length <= 0) {
      data.title = await transformTitle.promise([episodeData.episodeName], ctx);
    }
    if (fields.includes("aired") || fields.length <= 0) {
      const date = moment(episodeData.firstAired);
      const aired = await transformDate.promise(date, ctx);
      data.aired = aired.format("YYYY-MM-DD");
    }
    if (fields.includes("summary") || fields.length <= 0) {
      data.summary = await transformSummary.promise(episodeData.overview, ctx);
    }
    if (fields.includes("thumbnails") || fields.length <= 0) {
      const { filename } = episodeData;
      if (filename) {
        thumbnails = await transformThumbnails.promise(
          [`https://www.thetvdb.com/banners/${filename}`],
          ctx
        );
      }
    }
    return _.merge({}, rlt, { data, thumbnails });
  }

  protected async fetchEpisode(
    ctx: EpisodeContext
  ): Promise<TvdbQueryResponse> {
    const { season, episode, logger } = ctx;
    const { tvdbId } = this.option;

    try {
      const res = await this.axios.get<TvdbQueryResponse>(
        `/series/${tvdbId}/episodes/query`,
        {
          params: {
            airedSeason: season,
            airedEpisode: episode
          }
        }
      );
      return res.data;
    } catch (e) {
      logger.error("Failed to find episode", {
        label: "TvdbEpisodeScraper",
        error: e.response?.data?.Error,
        season,
        episode
      });
      throw e;
    }
  }

  protected async initAuthToken(ctx: EpisodeContext): Promise<void> {
    if (this.initToken) {
      return;
    }
    const { logger } = ctx;
    const {
      apiKey: apikey,
      userKey: userkey,
      userName: username
    } = this.option;
    try {
      const res = await this.axios.post<TvdbLoginResponse>("/login", {
        apikey,
        userkey,
        username
      });
      const { token } = res.data;
      this.axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
      this.initToken = true;
    } catch (e) {
      logger.error("Failed to fetch authentication token.", {
        label: "TvdbEpisodeScraper",
        error: e.response?.data?.Error
      });
      throw e;
    }
  }
}
